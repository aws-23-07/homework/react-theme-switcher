import Footer from "./Footer";
import Header from "./Header";
import Main from "./Main";
import React from "react";
import { ThemeProvider } from "./theme-context";

function App() {
  return (
    <ThemeProvider>
      <div className="min-h-screen flex flex-col">
        <Header />
        <Main />
        <Footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
